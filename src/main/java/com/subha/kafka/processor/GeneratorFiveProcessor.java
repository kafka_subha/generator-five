package com.subha.kafka.processor;

import org.apache.camel.Exchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Created by subha on 03/07/2018.
 */

@Component("genFive")
public class GeneratorFiveProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(GeneratorFiveProcessor.class);

    public void process(Exchange exchange) {
        int seed = Integer.parseInt(exchange.getIn().getBody().toString().split("-")[0]);
        long generatedResult = 2*seed + 10;

        LOG.info("Got seed " + seed);
        LOG.info("Generator one generated number " + generatedResult);

        exchange.getIn().setBody(generatedResult + "-GEN5-" + exchange.getIn().getBody().toString().split("-")[1]);
    }

}
